
#!/bin/bash
#Install the Necessary Packages
#To install all the necessary packages before the installation of Syncthing on Linux Mint 20, you have to execute the command shown below:
sudo apt-get update
sudo apt install nodejs npm -y
sudo apt-get install curl apt-transport-https -y


#Import the Syncthing GPG Key
#After installing these packages, you need to import the Syncthing GPG key with the following command:

curl –s https://syncthing.net/release-key.txt | sudo apt-key add – -y

#Create the Syncthing Repository
#Now, you should create a Syncthing repository with the help of the following command:

echo "deb https://apt.syncthing.net/ syncthing release" > sudo /etc/apt/sources.list.d/syncthing.list

# Allow the New Changes to take Effect
#After that, you should update your system to allow the new changes to take effect with the command shown below:
sudo apt-get update -y

#Install Syncthing on your System
#Once your system is updated with the new changes, you can install Syncthing on your system with the following command:
sudo apt-get install syncthing -y
# Confirm the Installation of Syncthing on your System
#You can confirm the installation of Syncthing on your system by executing the command shown below:
### version
syncthing --version
sudo systemctl enable syncthing@ubuntu.service
sudo systemctl start syncthing@ubuntu.service
#pour changer le port et IP
#nano ~/.config/syncthing/config.xml
#desinstaller syncthing
#sudo apt-get purge --autoremove syncthing
syncthing --version
sudo add-apt-repository ppa:ondrej/nginx -y && sudo apt update -y
sudo add-apt-repository ppa:ondrej/nginx-mainline -y && sudo apt update -y
sudo apt install nginx-core nginx-common nginx nginx-full -y
sudo rm -f /etc/nginx/sites-available/default
sudo mv -f default /etc/nginx/sites-available/
sudo chmod 644 /etc/nginx/sites-available/default
#sudo chmod 755 /etc/nginx/sites-available/
#sudo cat <<EOF >/etc/nginx/sites-available/syncthing.conf
#configuration syncthing
#server {
#  listen 80;
#  listen [::]:80 default_server;
#  access_log /var/log/nginx/syncthing.access.log;
#  error_log /var/log/nginx/syncthing.error.log;
#  location / {
#    proxy_pass http://localhost:8384;
#    proxy_set_header X-Real-IP $remote_addr;
#   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#    proxy_set_header X-Forwarded-Proto $scheme;
#  }
#}
#EOF
sudo systemctl reload nginx
sudo systemctl restart syncthing@ubuntu.service
sudo systemctl restart nginx
echo "installation synchthing terminée ,pour faire le test: Myip :8384 "
